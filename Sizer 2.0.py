#! /usr/bin/env python
#  -*- coding: utf-8 -*-
#
# GUI module generated by PAGE version 4.14
# In conjunction with Tcl version 8.6
#    Jun 04, 2018 03:58:12 PM

import sys
import os

try:
    from Tkinter import *
except ImportError:
    from tkinter import *

try:
    import ttk
    py3 = False
except ImportError:
    import tkinter.ttk as ttk
    py3 = True

import Gui_support

def vp_start_gui():
    '''Starting point when module is the main routine.'''
    global val, w, root
    root = Tk()
    top = New_Toplevel (root)
    Gui_support.init(root, top)
    root.mainloop()

w = None
def create_New_Toplevel(root, *args, **kwargs):
    '''Starting point when module is imported by another program.'''
    global w, w_win, rt
    rt = root
    w = Toplevel (root)
    top = New_Toplevel (w)
    Gui_support.init(w, top, *args, **kwargs)
    return (w, top)

def destroy_New_Toplevel():
    global w
    w.destroy()
    w = None


class New_Toplevel:
    def __init__(self, top=None):
        '''This class configures and populates the toplevel window.
           top is the toplevel containing window.'''
        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana1color = '#d9d9d9' # X11 color: 'gray85'
        _ana2color = '#d9d9d9' # X11 color: 'gray85'
        font9 = "-family {Segoe UI Black} -size 9 -weight bold -slant "  \
            "roman -underline 0 -overstrike 0"

        top.geometry("600x450+430+132")
        top.title("New Toplevel")
        top.configure(background="#4c4c4c")



        self.Frame1 = Frame(top)
        self.Frame1.place(relx=0.0, rely=0.0, relheight=0.11, relwidth=1.0)
        self.Frame1.configure(relief=GROOVE)
        self.Frame1.configure(borderwidth="2")
        self.Frame1.configure(relief=GROOVE)
        self.Frame1.configure(background="#4f4f4f")
        self.Frame1.configure(width=600)

        self.Label1 = Label(self.Frame1)
        self.Label1.place(relx=0.01, rely=0.28, height=21, width=186)
        self.Label1.configure(background="#4f4f4f")
        self.Label1.configure(disabledforeground="#9221a3")
        self.Label1.configure(font=font9)
        self.Label1.configure(foreground="#000000")
        self.Label1.configure(highlightcolor="#626263")
        self.Label1.configure(text='''Please Enter Folder Path Here :''')
        self.Label1.configure(width=186)

        self.Entry1 = Entry(self.Frame1)
        self.Entry1.place(relx=0.33, rely=0.3,height=20, relwidth=0.49)
        self.Entry1.configure(background="white")
        self.Entry1.configure(disabledforeground="#a3a3a3")
        self.Entry1.configure(font="TkFixedFont")
        self.Entry1.configure(foreground="#000000")
        self.Entry1.configure(insertbackground="black")
        self.Entry1.configure(width=294)

        self.Button1 = Button(self.Frame1, command=self.compute)
        self.Button1.place(relx=0.87, rely=0.26, height=24, width=67)
        self.Button1.configure(activebackground="#84d866")
        self.Button1.configure(activeforeground="#000000")
        self.Button1.configure(background="#d7bad8")
        self.Button1.configure(disabledforeground="#a3a3a3")
        self.Button1.configure(foreground="#000000")
        self.Button1.configure(highlightbackground="#2549d8")
        self.Button1.configure(highlightcolor="black")
        self.Button1.configure(pady="0")
        self.Button1.configure(text='''Compute''')
        self.Button1.configure(width=67)

        self.Text1 = Text(top)
        self.Text1.place(relx=0.03, rely=0.13, relheight=0.83, relwidth=0.94)
        self.Text1.configure(background="white")
        self.Text1.configure(font="TkTextFont")
        self.Text1.configure(foreground="black")
        self.Text1.configure(highlightbackground="#d9d9d9")
        self.Text1.configure(highlightcolor="black")
        self.Text1.configure(insertbackground="black")
        self.Text1.configure(selectbackground="#c4c4c4")
        self.Text1.configure(selectforeground="black")
        self.Text1.configure(width=564)
        self.Text1.configure(wrap=WORD)


    def compute(self):

        #Get enterd path
        text = self.Entry1.get()

        # Open a file
        TargetFolder = text

        # Get subFiles
        dirs = os.listdir(TargetFolder)

        # This would print all the files and directories
        for subFolder in dirs:

            # pick a folder you have ...
            folder_size = 0

            subFolderPath = os.path.join(TargetFolder, subFolder)

            for (path, dirs, files) in os.walk(subFolderPath):
                for file in files:
                    filename = os.path.join(path, file)
                    folder_size += os.path.getsize(filename)

            #Get size
            print(subFolder, "--->", "%0.1f MB" % (folder_size / (1024 * 1024.0)))
            textToPrint = subFolder, "--->", "%0.3f MB" % (folder_size / (1024 * 1024.0))
            self.Text1.insert(INSERT,textToPrint)
            self.Text1.insert(INSERT,"\n")



if __name__ == '__main__':
    vp_start_gui()




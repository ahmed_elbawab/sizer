import os, sys
import tkinter

# Open a file
TargetFolder = "D:\\Favourite"
TargetFolder = "D:\\Favourite\\A3RF"
# TargetFolder = "D:\\"

# Get subFiles
dirs = os.listdir( TargetFolder )

# This would print all the files and directories
for subFolder in dirs:

   # pick a folder you have ...
   folder_size = 0

   subFolderPath = os.path.join(TargetFolder, subFolder)

   for (path, dirs, files) in os.walk(subFolderPath):
       for file in files:
           filename = os.path.join(path, file)
           folder_size += os.path.getsize(filename)
   print( subFolder , "--->" , "%0.1f MB" % (folder_size / (1024 * 1024.0)))